const baseUrl = "http://localhost:8080";
let no = 1;

function getData() {
    fetch(`${baseUrl}/employees`)
        .then(response => response.json())
        .then(data => showData(data))
        .catch(console.error);
}

function getDataById(id) {
    fetch(`${baseUrl}/employee/${id}`)
        .then(response => response.json())
        .then(data => showDetail(data))
        .catch(console.error);    
}

function deleteData(id) {
    fetch(`${baseUrl}/delete/${id}`, {
        method: 'DELETE',
    })
    .then(res => location.reload())
    .catch(console.error);
}

function getByIdUpdate(id) {
  fetch(`${baseUrl}/employee/${id}`)
    .then(response =>  response.json())
    .then(data => updateData(data))
    .catch(console.error); 
}

function updateData(res) {
   const fieldUpdate = document.getElementById("selectUpdate").value;
   const inputUpdate = document.getElementById("inputUpdate").value;
   
  res[fieldUpdate] = inputUpdate;
  
  if (fieldUpdate != "") {
    fetch(`${baseUrl}/update`, {
      method: 'PUT',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(res)
    })
    .then(res => location.reload())
    .catch(console.error);
  }
 }


function changeUpdateField(sel) {
  const formUpdate = document.getElementById("formUpdate");
  const fieldUpdate = sel.options[sel.selectedIndex].text;
 
  if (fieldUpdate !== "Pilih..") {
      formUpdate.innerHTML = `<div id="selectedUpdate" style="font-family: PoppinsSemiBold;">${fieldUpdate}</div>
      <input type="text" class="form-control" id="inputUpdate">`
 }
}

function showData(data) {
    console.log(data)
    const cardList = document.getElementById("card-list");
    const totalKaryawan = document.getElementById("totalKaryawan");

    cardList.innerHTML = "";
      data.forEach(res => {
            cardList.innerHTML += `
            <div class="card mr-3" style="width: 18rem;" id="card">
            <div class="card-body" onclick="getDataById(${res.nik})" data-toggle="modal" data-target="#detailModal">
              <h5 class="card-title" id="nama">${res.nama}</h5>
              <h6 class="card-subtitle mb-2 text-muted" id="nik">${res.nik}</h6>
              <div class="card-text" id="ttl">${res.jenis_kelamin}</div>
              <div class="card-text" id="ttl">${res.tempat_tgl}</div>
            </div>
            <div class="card-body">
            <a href="#" class="card-link text-success" onclick="showUpdateModal(${res.nik})" data-toggle="modal" data-target="#updateModal">Update</a>
            <a href="#" class="card-link text-danger" onclick="showDeleteModal(${res.nik})" data-toggle="modal" data-target="#deleteModal">Delete</a></div>
          </div>`
          totalKaryawan.innerHTML = `Total Karyawan: ${no}`
          no++;
        }); 
}


function showUpdateModal(id) {
    const updateModal = document.getElementById("updateModal");
    updateModal.innerHTML = `<div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header">
        <h4 class="modal-title">Update</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div id="nikModal" style="font-family: PoppinsSemiBold;">NIK: ${id}</div>
         <select class="form-control" style="width: 50%; margin-top: 10px; margin-bottom: 20px;" id="selectUpdate" onclick="changeUpdateField(this)">
          <option>Pilih..</option>
          <option value="nama">Nama</option>
          <option value="tempat_tgl">Tempat/Tanggal Lahir</option>
          <option value="alamat">Alamat</option>
          <option value="jenis_kelamin">Jenis Kelamin</option>
          <option value="rt_rw">Rt/Rw</option>
          <option value="kel_desa">Kel/Desa</option>
          <option value="kecamatan">Kecamatan</option>
          <option value="agama">Agama</option>
          <option value="status">Status</option>
          <option value="pekerjaan">Pekerjaan</option>
          <option value="kewarganegaraan">Kewarganegaraan</option>
        </select>
        <div class="form-group" id="formUpdate">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="getByIdUpdate(${id})">Submit</button>
      </div>
    </div>
  </div>`;
}

function showDeleteModal(id) { 
    const deleteModal = document.getElementById("deleteModal");
    deleteModal.innerHTML = `
    <div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header">
        <h4 class="modal-title">Delete</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <p class="ml-3" style="font-size: 20px; padding:10px">Delete data with NIK <b>${id}</b>?</p>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" onclick="deleteData(${id})">Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>`
}


function showDetail(data) {
    const modalDetail = document.getElementById("detailModal");
    modalDetail.innerHTML = `
    <div class="modal-dialog">
              <div class="modal-content">
                 <div class="modal-header">
                  <h4 class="modal-title">Detail Data</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">NIK: ${data.nik}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Nama: ${data.nama}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Tempat/Tanggal Lahir: ${data.tempat_tgl}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Jenis Kelamin: ${data.jenis_kelamin}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Alamat: ${data.alamat}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">RT/RW: ${data.rt_rw}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Kel/Desa: ${data.kel_desa}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Kecamatan: ${data.kecamatan}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Agama: ${data.agama}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Status: ${data.status}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Pekerjaan: ${data.pekerjaan}</div> 
                  <div id="nikModal" style="font-family: PoppinsMedium; margin-bottom: 20px;">Kewarganegaraan: ${data.kewarganegaraan}</div> 
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>`
}
 
getData();