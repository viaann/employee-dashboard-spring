function insertData() {
    let jsonReq = "";
    const nik = document.getElementById("nikInput").value;
    const nama = document.getElementById("namaInput").value;
    const tempatLahir = document.getElementById("tempatInput").value;
    const jenisKelamin = document.getElementById("jenisKelaminSelect").value;
    const alamat = document.getElementById("alamatInput").value;
    const rtRw = document.getElementById("rtRwInput").value;
    const kelDesa = document.getElementById("kelDesaInput").value;
    const kecamatan = document.getElementById("kecInput").value;
    const agama = document.getElementById("agamaSelect").value;
    const status = document.getElementById("statusSelect").value;
    const pekerjaan = document.getElementById("pekerjaanInput").value;
    const kewarganegaraan = document.getElementById("wargaInput").value;

    
    if (jenisKelamin != "Pilih.." && agama != "Pilih.." && status != "Pilih..") {
        jsonReq = JSON.stringify({
            nik: parseInt(nik),
            nama: nama,
            tempat_tgl: tempatLahir,
            alamat: alamat,
            rt_rw: rtRw,
            kel_desa: kelDesa,
            kecamatan: kecamatan,
            jenis_kelamin: jenisKelamin,
            agama: agama,
            status: status,
            pekerjaan: pekerjaan,
            kewarganegaraan: kewarganegaraan
        })
        postData(jsonReq);
    } else {
        alert("Data yang dimasukan salah");
    }  
}

function postData(body) {
    fetch(`http://localhost:8080/addEmployee`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: body
    })
    .then(res => {
        alert("Data yang dimasukan benar");
        location.reload();
     })
    .then(data => console.log(data))
    .catch(console.error);
}