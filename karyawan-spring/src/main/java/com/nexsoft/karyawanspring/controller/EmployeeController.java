package com.nexsoft.karyawanspring.controller;

import com.nexsoft.karyawanspring.entity.Employee;
import com.nexsoft.karyawanspring.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @PostMapping("/addEmployee")
    public Employee addProduct(@RequestBody Employee employee) {
        return service.saveEmployee(employee);
    }

    @GetMapping("/employees")
    public List<Employee> findAllEmployee() {
        return service.getProducts();
    }

    @GetMapping("/employee/{id}")
    public Employee findEmployeeById(@PathVariable int id) {
        return service.getProductById(id);
    }

    @PutMapping("/update")
    public Employee updateProduct(@RequestBody Employee product) {
        return service.updateProduct(product);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id) {
        return service.deleteProduct(id);
    }
}
