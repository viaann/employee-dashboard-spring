package com.nexsoft.karyawanspring.service;

import com.nexsoft.karyawanspring.entity.Employee;
import com.nexsoft.karyawanspring.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository repository;

    public Employee saveEmployee(Employee employee) {
        return repository.save(employee);
    }

    public List<Employee> saveEmployees(List<Employee> employees) {
        return repository.saveAll(employees);
    }

    public List<Employee> getProducts() {
        return repository.findAll();
    }

    public Employee getProductById(int id) {
        return repository.findById(id).orElse(null);
    }

    public String deleteProduct(int id) {
        repository.deleteById(id);
        return "Employee removed!!";
    }

    public Employee updateProduct(Employee employee) {
        Employee existingProduct = repository.findById(employee.getNik()).orElse(null);
        existingProduct.setNama(employee.getNama());
        existingProduct.setAlamat(employee.getAlamat());
        existingProduct.setAgama(employee.getAgama());
        existingProduct.setJenis_kelamin(employee.getJenis_kelamin());
        existingProduct.setKecamatan(employee.getKecamatan());
        existingProduct.setTempat_tgl(employee.getTempat_tgl());
        existingProduct.setKel_desa(employee.getKel_desa());
        existingProduct.setKewarganegaraan(employee.getKewarganegaraan());
        existingProduct.setStatus(employee.getStatus());
        existingProduct.setPekerjaan(employee.getPekerjaan());
        return repository.save(existingProduct);
    }
}
