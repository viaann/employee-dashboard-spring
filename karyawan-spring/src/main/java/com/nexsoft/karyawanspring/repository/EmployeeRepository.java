package com.nexsoft.karyawanspring.repository;

import com.nexsoft.karyawanspring.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
