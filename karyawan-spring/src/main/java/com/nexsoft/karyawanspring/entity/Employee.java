package com.nexsoft.karyawanspring.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "data")
public class Employee {
    @Id
    private int nik;
    private String nama;
    private String tempat_tgl;
    private String jenis_kelamin;
    private String alamat;
    private String rt_rw;
    private String kel_desa;
    private String kecamatan;
    private String agama;
    private String status;
    private String pekerjaan;
    private String kewarganegaraan;
}
