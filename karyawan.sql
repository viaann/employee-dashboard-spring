-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Jul 2020 pada 10.35
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data`
--

CREATE TABLE `data` (
  `nik` int(10) NOT NULL,
  `nama` text NOT NULL,
  `tempat_tgl` text NOT NULL,
  `jenis_kelamin` text NOT NULL,
  `alamat` text NOT NULL,
  `rt_rw` varchar(50) NOT NULL,
  `kel_desa` text NOT NULL,
  `kecamatan` text NOT NULL,
  `agama` text NOT NULL,
  `status` text NOT NULL,
  `pekerjaan` text NOT NULL,
  `kewarganegaraan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data`
--

INSERT INTO `data` (`nik`, `nama`, `tempat_tgl`, `jenis_kelamin`, `alamat`, `rt_rw`, `kel_desa`, `kecamatan`, `agama`, `status`, `pekerjaan`, `kewarganegaraan`) VALUES
(222, 'Jetty', 'Tokyo, 22 September 1999', 'Perempuan', 'asas', 'asas', 'asas', 'asas', 'Islam', 'Kawin', 'asasas', 'asas'),
(30123414, 'Reyna', 'Jakarta, 20 Agustus 1969', 'Laki-Laki', 'Casa Jardin', '002/001', 'Cengkareng', 'Cengkareng', 'Islam', 'Belum Kawin', 'Others', 'Indonesia'),
(33333333, 'as', 'as', 'Laki Laki', 'ya', '002/001', 'ya', 'as', 'Katolik', 'Kawin', 'ya', 'ya');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`nik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
